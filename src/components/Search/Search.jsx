import styles from './Search.module.scss';
import { ReactComponent as SearchIcon } from './icons/search.svg';

const Search = () => {
  return (
    <div className={ styles.searchWrapper}>
      <SearchIcon />
      <input type="text"  placeholder='Search'/>
    </div>
  );
};

export default Search;
