import { NavLink } from 'react-router-dom';
import styles from './NavBar.module.scss';

const NavBar = () => {
  return (
    <nav className={styles.nav}>
      <ul>
        <li>
          <NavLink to="shop" className={({isActive}) => isActive ? `${styles.activeLink}` : ''}>Shop</NavLink>
        </li>
        <li>
          <NavLink to="men" className={({isActive}) => isActive ? `${styles.activeLink}` : ''}>Men</NavLink>
        </li>
        <li>
          <NavLink to="women" className={({isActive}) => isActive ? `${styles.activeLink}` : ''}>Women</NavLink>
        </li>
        <li>
          <NavLink to="combos" className={({isActive}) => isActive ? `${styles.activeLink}` : ''}>Combos</NavLink>
        </li>
        <li>
          <NavLink to="joggers" className={({isActive}) => isActive ? `${styles.activeLink}` : ''}>Joggers</NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default NavBar;
